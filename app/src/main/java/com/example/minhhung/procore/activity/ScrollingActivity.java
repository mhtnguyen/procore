package com.example.minhhung.procore.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.example.minhhung.procore.R;

public class ScrollingActivity extends AppCompatActivity {
    TextView left, right;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_diff);
        left = (TextView) findViewById(R.id.left_side);
        if(left!=null) {
            left.setMovementMethod(new ScrollingMovementMethod());
        }
        right = (TextView) findViewById(R.id.right_side);
        if(right!=null){
            right.setMovementMethod(new ScrollingMovementMethod());
        }
        String s = getIntent().getStringExtra("DIFF_URL");
        if(s!=null) {
            left.setText(s.replaceAll("(?m)^\\+.*", ""));
            right.setText(s.replaceAll("(?m)^-.*", ""));
        }else{
            left.setText("no deletions found!");
            right.setText("no additions found!");
        }
    }
}
