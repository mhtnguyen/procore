package com.example.minhhung.procore.model;

/**
 * Created by minhhung on 10/31/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by minhhung on 2/5/2017.
 */
public class PullRequests {
    @SerializedName("pullRequests")
    @Expose
    private ArrayList<PullRequest> pullRequests = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public PullRequests() {
    }

    public PullRequests(ArrayList<PullRequest> pullRequests) {
        super();
        this.pullRequests = pullRequests;
    }

    public ArrayList<PullRequest> getPullRequests() {
        return pullRequests;
    }

    public void setResults(ArrayList<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }


}
