package com.example.minhhung.procore.activity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    private Rect rect;
    private Paint paint;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        rect = new Rect();
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setTextSize(24);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int baseline = getBaseline();
        for (int i = 0; i < getLineCount(); i++) {
            if(this.getText()!=null && this.getText().toString().matches("^\\+.*")){
                paint.setColor(Color.GREEN);
            }
            else if(this.getText()!=null && this.getText().toString().matches("^\\-.*")){
                paint.setColor(Color.RED);
            }else{
                paint.setColor(Color.BLACK);
            }
            canvas.drawText("" + (i+1), rect.left, baseline, paint);

            baseline += getLineHeight();
        }
        super.onDraw(canvas);
    }
}
