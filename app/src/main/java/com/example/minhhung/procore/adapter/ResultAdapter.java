package com.example.minhhung.procore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.minhhung.procore.R;
import com.example.minhhung.procore.model.PullRequest;
import com.example.minhhung.procore.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ResultAdapter extends ArrayAdapter<PullRequest> {

    List<PullRequest> resultLists;
    Context context;
    private LayoutInflater mInflater;

    public ResultAdapter(Context context, List<PullRequest> objects) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        resultLists = objects;
    }

    @Override
    public PullRequest getItem(int position) {
        return resultLists.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.layout_row_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        PullRequest item = getItem(position);
        if(item !=null){
            String title = item.getTitle();
            if(title != null){
                vh.textViewTitle.setText(title);
            }
            String url = item.getUrl();
            if(url !=  null) {
                vh.textViewPR.setText(url);
            }
            User user = item.getUser();
            if(user!=null) {
                Picasso.with(context).load(user.getAvatarUrl()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);
            }
        }

        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final ImageView imageView;
        public final TextView textViewTitle;
        public final TextView textViewPR;

        private ViewHolder(RelativeLayout rootView, ImageView imageView, TextView textViewTitle, TextView textViewPR) {
            this.rootView = rootView;
            this.imageView = imageView;
            this.textViewTitle = textViewTitle;
            this.textViewPR = textViewPR;
        }

        public static ViewHolder create(RelativeLayout rootView) {
            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            TextView textViewTitle = (TextView) rootView.findViewById(R.id.textViewTitle);
            TextView textViewPR = (TextView) rootView.findViewById(R.id.textViewPR);
            return new ViewHolder(rootView, imageView, textViewTitle, textViewPR);
        }
    }
}
