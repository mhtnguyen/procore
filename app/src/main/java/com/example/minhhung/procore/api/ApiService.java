package com.example.minhhung.procore.api;

import com.example.minhhung.procore.model.PullRequest;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiService {

    @GET("/repos/JakeWharton/butterknife/pulls")
    Call<List<PullRequest>> getPullRequests(@QueryMap Map<String, String> params);
}