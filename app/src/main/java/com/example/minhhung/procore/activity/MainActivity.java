package com.example.minhhung.procore.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.minhhung.procore.Application.App;
import com.example.minhhung.procore.R;
import com.example.minhhung.procore.adapter.ResultAdapter;
import com.example.minhhung.procore.api.ApiService;
import com.example.minhhung.procore.model.PullRequest;
import com.example.minhhung.procore.util.InternetConnection;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    /**
     * Views
     */
    private ListView listView;
    private View parentView;

    private ResultAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        parentView = findViewById(R.id.parentLayout);

        /**
         * Getting List and Setting List Adapter
         */
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String diffUrl = adapter.getItem(position).getDiffUrl();
                new GetData().execute(diffUrl);
            }
        });

        loadJSON();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fab.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_menu_rotate, MainActivity.this.getTheme()));
        }
        else {
            fab.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_menu_rotate));
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull final View view) {

                loadJSON();
            }
        });
    }

    private void loadJSON(){
        if (InternetConnection.checkConnection(getApplicationContext())) {
            final ProgressDialog dialog;
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setTitle(getString(R.string.string_getting_json_title));
            dialog.setMessage(getString(R.string.string_getting_json_message));
            dialog.show();
            //Creating an object of our api interface
            ApiService api = App.getClient().getApiService();
            Map<String, String> params = new HashMap<String, String>();
            params.put("state", "open");

            Call<List<PullRequest>> call = api.getPullRequests(params);

            call.enqueue(new Callback<List<PullRequest>>() {
                @Override
                public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                    dialog.dismiss();

                    if(response.isSuccessful()) {
                        List<PullRequest> rs = response.body();
                        /**
                         * Binding that List to Adapter
                         */
                        adapter = new ResultAdapter(MainActivity.this, rs);
                        listView.setAdapter(adapter);

                    } else {
                        Snackbar.make(parentView, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                    dialog.dismiss();
                }
            });

        } else {
            Snackbar.make(parentView, R.string.string_internet_connection_not_available, Snackbar.LENGTH_LONG).show();
        }
    }

    class GetData extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setProgress(0);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            String result = "";
            try {
                if(params[0]!=null && !params[0].isEmpty()) {

                    URL url = new URL(params[0]);
                    urlConnection = (HttpURLConnection) url.openConnection();

                    int code = urlConnection.getResponseCode();

                    if (code == 200) {
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        if (in != null) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                            String line = "";

                            while ((line = bufferedReader.readLine()) != null) {
                                result += line + "\n\n";
                            }
                        }
                        in.close();
                    }
                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            finally {
                urlConnection.disconnect();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {

            if(progressDialog!=null){
                progressDialog.dismiss();
            }
            if(result!=null && result.length()>0){
                Intent intent = new Intent(getApplicationContext(),ScrollingActivity.class);
                intent.putExtra("DIFF_URL",result);
                startActivity(intent);
            }else{
                Snackbar.make(parentView, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();
            }
        }
    }
}
