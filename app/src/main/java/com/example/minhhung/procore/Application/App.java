package com.example.minhhung.procore.Application;

import android.app.Application;

import com.example.minhhung.procore.api.Client;


public class App extends Application {
    private Client client;
    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        client = new Client(mInstance);
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public Client getClientInstance() {
        return this.client;
    }

    public static Client getClient() {
        return getInstance().getClientInstance();
    }
}
